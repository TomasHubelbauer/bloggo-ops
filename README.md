# Ops

> My journey into collecting links and studying deployment as practiced by other
> people.

## Glossary

- [ ] Mark section for each linked tech with what it is from the *asS list

- PaaS: Platform as a service
- SaaS: Service as a service
- FaaS: Function as a service
- Iass: Infrastructure as a service

## Dokku

Dokku is like Heroku but on your on server

- [GitLab repository](https://gitlab.com/TomasHubelbauer/bloggo-dokku)
- [Bloggo post](http://hubelbauer.net/post/dokku)

## Flynn

Flynn is like Dokku but with high availability

- [GitLab repository](https://gitlab.com/TomasHubelbauer/bloggo-flynn)
- [Bloggo post](http://hubelbauer.net/post/flynn)

- [ ] Find out if Flynn is powered by Kubernetes

## Deis

Deis (Workflow) is like Flynn, not sure about the difference yet, but it is build on Kubernetes

- [GitLab repository](https://gitlab.com/TomasHubelbauer/bloggo-flynn)
- [Bloggo post](http://hubelbauer.net/post/flynn)

## Gitkube

Gitkube is for `git push`ing stuff to a Kubernetes cluster as if it was a remote

- [GitLab repository](https://gitlab.com/TomasHubelbauer/bloggo-gitkube)
- [Bloggo post](http://hubelbauer.net/post/gitkube)

## Kubernetes

- [GitLab repository](https://gitlab.com/TomasHubelbauer/bloggo-k8s)
- [Bloggo post](http://hubelbauer.net/post/k8s)

## Azure

- [GitLab repository](https://gitlab.com/TomasHubelbauer/bloggo-azure)
- [Bloggo post](http://hubelbauer.net/post/azure)

- [ ] Add some notes on Azure

## AWS

- [GitLab repository](https://gitlab.com/TomasHubelbauer/bloggo-aws)
- [Bloggo post](http://hubelbauer.net/post/aws)

- [ ] Add some notes on AWS

## Google Cloud Platform (GCP)

- [ ] Add some notes on GCP

## Open Source DDOS Protection

- [Robbo](https://github.com/yuri-gushin/Roboo) filters out robots by requiring connections to set a HTTP, JS and SWF cooki, which most robots won't
- [GateKeeper](https://github.com/AltraMayor/gatekeeper) probably just keeps a log of IP addresses and aborts frequent connections from a single address

## Unannotated articles

- [ ] Annonate

### [How to Deploy Software](https://zachholman.com/posts/deploying-software) by Zach Holman
